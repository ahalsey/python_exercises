def days_to_units(num_of_days, conversion_unit):
    if conversion_unit == "hours":
        return f"{num_of_days} days are {num_of_days * 24} {conversion_unit}"
    elif conversion_unit == "minutes":
        return f"{num_of_days} days are {num_of_days * 24 * 60} {conversion_unit}"
    else:
        return "Unsupported unit, enter hours or minutes only."

def validate():
    try:
        user_input_num = int(days_and_unit_dict["days"])
        if user_input_num > 0:
            calculated_value = days_to_units(user_input_num, days_and_unit_dict["unit"])
            print(calculated_value)
        else:
            print("Enter a positive number.\n")
    except ValueError:
        print("Invalid input!\n")

user_input = ""

while user_input != "exit":
    user_input = input("Enter a number of days and units separated by a colon: ")
    days_and_unit = user_input.split(":")
    days_and_unit_dict = {"days": days_and_unit[0], "unit": days_and_unit[1]}
    print(days_and_unit_dict)
    validate()