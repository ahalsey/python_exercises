class User:
    def __init__(self, params):
        self.email = params[0]
        self.name = params[1]
        self.pwd = params[2]
        self.job_title = params[3]

    def change_pwd(self, new_pwd):
        self.pwd = new_pwd

    def change_job_title(self, new_job_title):
        self.job_title = new_job_title

    def get_user_info(self):
        print(f"User {self.name} currently works as a {self.job_title}. You can contact them at {self.email}.")



app_user_one = User(["nn@nn.com", "Nana", "pwd1", "DevOps Engineer"])
app_user_one.get_user_info()

user_input = []
user_input.append(input("Enter your email: "))
user_input.append(input("Enter your name: "))
user_input.append(input("Enter a password: "))
user_input.append(input("Enter your job title: "))
app_user_two = User(user_input)
app_user_two.get_user_info()