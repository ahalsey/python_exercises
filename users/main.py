from user import User
from post import Post

app_user_one = User(["nn@nn.com", "Nana", "pwd1", "DevOps Engineer"])
app_user_one.get_user_info()

user_input = []
user_input.append(input("Enter your email: "))
user_input.append(input("Enter your name: "))
user_input.append(input("Enter a password: "))
user_input.append(input("Enter your job title: "))
app_user_two = User(user_input)
app_user_two.get_user_info()

new_post = Post("On a secret mission", app_user_two.name)
new_post.get_post_info()