def calculate(a,b,operation):
    if operation == "+":
        solution = a + b
    elif operation == "-":
        solution = a - b
    elif operation == "*":
        solution = a * b
    elif operation == "/":
        solution = a / b
    elif operation == "%":
        solution = a % b

    print(f"{a} {operation} {b} is {solution}")

def validate(n,m,operation):
    try:
        if operation in ["+","-","*","/","%"]:
            calculate(n,m,operation)
        else:
            print("Enter numbers and valid operators only!")
    except ValueError:
        print("Invalid input!")

user_input_nums = ""
while user_input_nums != "exit":
    try:
        user_input_nums = input("Enter two numbers separated by a comma: ")
        if user_input_nums.lower() == "exit":
            sys.exit(0)
        else:
            numbers = user_input_nums.split(",")
            num0, num1 = int(numbers[0]), int(numbers[1])
    except: 
        print("Enter numbers only!")
        continue
    user_input_operator = input("Enter desired operation (+,-,*,/,%): ")
    validate(num0, num1, user_input_operator)