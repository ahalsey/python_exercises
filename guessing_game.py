import random

count = 0

guess = 0

rand = random.randint(1,100)
while guess != rand:
    try:
        guess = int(input("Enter an integer between 1 and 100: "))
    except ValueError:
        print("Invalid input! Enter an integer only.")
        continue

    if guess > rand:
        print("Too high! Guess a lower number.\n")
    elif guess < rand:
        print("Too low! Guess a higher number.\n")
    
    count += 1

print(f"Correct! The answer is {rand}. You guessed {count} times.")