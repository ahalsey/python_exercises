import random

rando = random.randint(1,100)

guess = int()

while guess != rando:
    try:
        guess = int(input("Guess a number 1-100: "))
    except:
        print("Enter an integer!\n")
        continue

    if guess == rando:
        print("You won!\n")
    elif guess < rando:
        print("You guessed too low. Try again.\n")
    elif guess > rando:
        print("You guessed too high. Try again.\n")
