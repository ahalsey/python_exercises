import openpyxl

inv_file = openpyxl.load_workbook("inventory.xlsx")
product_list = inv_file["Sheet1"]

products_per_supplier = {}
total_value_per_supplier = {}
products_under_10_inv = {}

for item in range(2, product_list.max_row + 1):
    supplier_name = product_list.cell(item, 4).value
    inventory = product_list.cell(item, 2).value
    price = product_list.cell(item, 3).value
    product_num = product_list.cell(item, 1).value
    inventory_price = product_list.cell(item,5)

    # calculate number of products per supplier and value of inventory per supplier
    if supplier_name in products_per_supplier:
        products_per_supplier[supplier_name] += 1
        total_value_per_supplier[supplier_name] += inventory * price
    else: 
        print("adding new supplier")
        products_per_supplier[supplier_name] = 1
        total_value_per_supplier[supplier_name] = inventory * price

    # find total value of inventory under 10
    if inventory < 10:
        products_under_10_inv[product_num] = inventory


    # add value for total inventory price
    inventory_price.value = inventory * price

print(products_per_supplier)
print(total_value_per_supplier)
print(products_under_10_inv)

inv_file.save("inventory.xlsx")